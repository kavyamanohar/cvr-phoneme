This is a [kaldi](https://kaldi-asr.org/) based recipe for phoneme recognition. Eventhough the recipe is originally written for studying the effects of **consonant-vowel ratio (cvr)** modification in phoneme recovey from noisy Malayalam speech, the script is generic and can be adapted for phoneme recognition in any language if you format the input directory as described here.

You need a working Kaldi directory to run this script.

Details on how to run this script and the working is described here.

To install Kaldi, see the documentation [here](https://kaldi-asr.org/doc/install.html)

The source code of `/cvr_phoneme` has to be placed in the `/egs` directory of Kaldi installation directory.

## USAGE

`./run_gmm.sh ./inputdirectory`

input directory has the following structure:
```
├── language -> symlink/to/language/model/source ( lm_train.txt, lexicon.txt)
├── test
│   └── corpus1
│       ├── audio -> symlink/to/audio/files_directory (utt1.wav, utt2.wav)
│       └── metadata.tsv -> symlink/to/audio/files/metadata (metadata.tsv)
└── train
    ├── corpus2
    |      ├── audio -> symlink/to/audio/files_directory (utt1.wav, utt2.wav)
    |      └── metadata.tsv -> symlink/to/audio/files/metadata (metadata.tsv)
    └── corpus3
        ├── audio -> symlink/to/audio/files_directory (utt1.wav, utt2.wav)
        └── metadata.tsv -> symlink/to/audio/files/metadata (metadata.tsv)
```
### INPUT FILE DESCRIPTION

Since the aim of this recipe is to obtain phoneme transcripts, instead of word level language model, a **phototactic model** (As described in [this paper](https://dl.acm.org/doi/abs/10.1016/j.csl.2022.101358)) is to be trained. The `lm_train.txt` contains regular Malayalam sentences transcribed as phonemes. 

For example the sentence: **പൂജ്യം പൂജ്യം ഒമ്പത് എട്ട് ഏഴ് ആറ് അഞ്ച്** is transcribed as **p uː ɟ j a m p uː ɟ j a m o m p a t̪ ə e ʈ ʈ ə eː ɻ ə aː r ə a ɲ t͡ʃ ə** . This PA based phoneme level transcription is obtained using [Mlphon](https://pypi.org/project/mlphon/). The statistical n-gram phonotactic model is trained using regular word level language modeling toolkit SRILM assuming phonemes as word like units.

A pronunciation lexicon generally describes the pronunciation of a word as a sequnce of phonemes. In this recipe, since phonemes are assumed to be word like units, a dummy lexicon of the follwing structure is used.

```
a       a
aː      aː
e       e
eː      eː
ə       ə
j       j
ɟ       ɟ
l       l
```

metadata.tsv is a tab separated values of utterence_id, speaker_id, file_name in audio folder, transcript as a sequence of phonemes separated by spaces. Since we do not intend to recover words, no word separator is used.

Example entry in metadata.tsv:

```
ML001_f0001     ML001   ML001_f0001.wav p uː ɟ j a m p uː ɟ j a m o m p a t̪ ə e ʈ ʈ ə eː ɻ ə aː r ə a ɲ t͡ʃ ə
```

The audio files are expected to be in .wav file format.